"use strict";
const express = require('express');
const path = require('path');
require('dotenv').config();
const cors = require('cors');
const app = express();
require('./App/DB.js');
//let whitelist = [process.env.Base_Url, '']

// app.use(cors({
// 	origin: function(origin, callback) {
// 		if(!origin) return callback(null, true);
// 		if(whitelist.indexOf(origin) === -1) {
// 			let message = 'The CORS policy for this origin does not allow access from the particular origin.';
// 			console.log(message);
// 			return callback(new Error(message), false);
// 		}
// 		return callback(null, true);
// 	}
// }));

require('dotenv-flow').config({
  path: './env/'
});
console.log(" process.env.MG_HOST " , process.env.MG_HOST);
// app.use(session({
// 	secret: "niftySession",
// 	resave: true,
// 	saveUninitialized: true,
// 	cookie: {
// 		maxAge: 1000 * 60 * 60 * 24 * 7
// 	}
// }));

app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
require('./App/routes/index')(app);

const Port = process.env.PORT_NUMBER || 3000;
app.listen(Port, () => {
	console.log("Server Listing at :- " + Port + "");
})