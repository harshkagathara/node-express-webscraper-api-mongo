const path = require("path");
const fs = require("fs");
const XMLHttpRequest = require("xhr2");
const request = require("request");
const Scrapp = require("../schema/scrapp");
const ScrappedData = require("../schema/scrappedData");

const cheerio = require("cheerio");
const xl = require("excel4node");
const wb = new xl.Workbook();
const ws = wb.addWorksheet("Worksheet Name");
const ObjectID = require("mongodb").ObjectID;

class IndexModel {

  async getDataFromXlsxFile() {
    let promise = new Promise(async function(resolve, reject) {
      const reader = require("xlsx");
      const file = reader.readFile(path.join(__dirname, `../../public/Amazon-Playlists.xlsx`));
      let data = [];
      let url = [];
      const sheets = file.SheetNames;
      for(let i = 0; i < sheets.length; i++) {
        const temp = reader.utils.sheet_to_json(file.Sheets[file.SheetNames[i]]);
        temp.forEach((res) => {
          data.push(res);
        });
      }
      data.forEach((element) => {
        url.push(element.LINK);
      });
      resolve(url);
    });
    return promise;
  }

  async checkUrlValidOrNot(urlData) {
    let promise = new Promise(async function(resolve, reject) {
      let url = urlData;
      let request = new XMLHttpRequest();
      request.open("GET", url, true);
      request.send();
      request.onload = async() => {
        let urlData = new Object();
        urlData.status = request.status;
        urlData.url = url;
        console.log(request.status, "Valid URL -> ", url);
        resolve(urlData);
      };
    });
    return promise;
  }

  async CreateSitemap(url) {
    let promise = new Promise(async function(resolve, reject) {
      const data = {
        _id: Date.now(),
        startUrl: [url],
        selectors: [{
          id: "title",
          parentSelectors: ["_root"],
          type: "SelectorHTML",
          selector: "div._2Q3_4s0NzM6T4F3OMZP4lB",
          multiple: false,
          regex: "",
          delay: 0,
        }, ],
      };
      let requestContent = {
        url: "https://api.webscraper.io/api/v1/sitemap?api_token=MAGNtHEXFM8WEJbJ68IMsusQpwcI635yEMUNwzCA8ZCRF1fA8z3XSu6mz5My",
        method: "POST",
        body: data,
        json: true,
      }; {
        request.post(requestContent, function(error, response, body) {
          if(response.body.error != undefined) {
            console.log(error);
          }
          resolve(response.body);
        });
      }
    });
    return promise;
  } 

  async updatedcrapingJob() {
    let promise = new Promise(async function(resolve, reject) {

      const todayDate_ob = new Date();
      const todayDay = ("0" + todayDate_ob.getDate()).slice(-2);
      const todayMonth = ("0" + (todayDate_ob.getMonth() + 1)).slice(-2);
      const todayYear = todayDate_ob.getFullYear();
      const todayDate = (todayYear + "-" + todayMonth  + "-" + todayDay).toString() ;
   
      const nameData_ob = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
      const nextDay = nameData_ob.getDate()
      const nextMonth = nameData_ob.getMonth() + 1
      const nextYear = nameData_ob.getFullYear()
      const nextDate =  (nextYear + "-" + nextMonth + "-" + nextDay  ).toString();

      const query = {
        createdAt: {
          $gte: new Date(todayDate).toISOString(),
          $lt: new Date(nextDate).toISOString(),
        },
        isDeleted: 0,
        status: "finished",
      };
      Scrapp.find(query, function(err, data) {
        if(err) {
          console.log("Error during fetching apiinfo : " + err);
        }
        resolve(data);
      });
    });
    return promise;
  }

  async SaveScrepData(jsonData) {
    let promise = new Promise(async function(resolve, reject) {
      new Scrapp(jsonData).save((err, doc) => {
        if(!err) resolve(doc._id);
        else {
          console.log("Error during saving poi info : " + err);
        }
      });
    });
    return promise;
  }

  async CreateScrapingJob(id, url) {
    let promise = new Promise(async function(resolve, reject) {
      const data = {
        sitemap_id: id,
        driver: "fulljs",
        page_load_delay: 10000,
        request_interval: 10000,
        proxy: "No",
        start_urls: [url],
        custom_id: "custom-scraping-job-12",
      };
      let requestContent = {
        url: "https://api.webscraper.io/api/v1/scraping-job?api_token=MAGNtHEXFM8WEJbJ68IMsusQpwcI635yEMUNwzCA8ZCRF1fA8z3XSu6mz5My",
        method: "POST",
        body: data,
        json: true,
      }; {
        request.post(requestContent, function(error, response, body) {
          if(response.body.error != undefined) {
            console.log(error);
          }
          resolve(response.body);
        });
      }
    });
    return promise;
  }

	

  async savaScrappedData(ScrapingJobEle,downloadScrapedData) {
    let promise = new Promise(async function(resolve, reject) {

      downloadScrapedData.idScrapp = ScrapingJobEle._id;
    
      new ScrappedData(downloadScrapedData).save((err, doc) => {
        if(!err) resolve(doc._id);
        else {
          console.log("Error during saving poi info : " + err);
        }
      });
    });
    return promise;
  }

  async readScrappData() {
    let promise = new Promise(async function(resolve, reject) {
      const query = {
        createdAt: {
          $gte: new Date("2022-04-28").toISOString(),
          $lt: new Date("2022-04-29").toISOString(),
        },
        isDeleted: 0,
      };
      Scrapp.find(query, function(err, data) {
        if(err) {
          console.log("Error during fetching apiinfo : " + err);
        }
        resolve(data);
      });
    });
    return promise;
  }
  async GetScrapingJob(CreateScrapingJob) {
    let promise = new Promise(async function(resolve, reject) {
      let requestContent = {
        url: "https://api.webscraper.io/api/v1/scraping-job/" + CreateScrapingJob + "?api_token=MAGNtHEXFM8WEJbJ68IMsusQpwcI635yEMUNwzCA8ZCRF1fA8z3XSu6mz5My",
        json: true,
      };
      request.get(requestContent, async function(error, response, body) {
        if(error) {
          console.log(error);
        }
        resolve(body);
        // console.log(body);
      });
    });
    return promise;
  }

  async updateStatus(allData, status) {
    let promise = new Promise(async function(resolve, reject) {
      let query = {
        _id: ObjectID(allData._id),
      };
      Scrapp.findOneAndUpdate(query, {
        $set: {
          status: status,
        },
      }, {
        new: true,
      }, function(err, doc) {
        if(err) {
          console.log("Error during upadteing last sync date : " + err);
        } else {
          resolve(doc);
        }
      });
    });
    return promise;
  }

  async updateIsDelete(ScrapingJobEle) {
    let promise = new Promise(async function(resolve, reject) {
      let query = {
        _id: ObjectID(ScrapingJobEle._id),
      };
      Scrapp.findOneAndUpdate(query, {
        $set: {
          isDeleted: 1,
        },
      }, {
        new: true,
      }, function(err, doc) {
        if(err) {
          console.log("Error during upadteing last sync date : " + err);
        } else {
          resolve(doc);
        }
      });
    });
    return promise;
  }

  async downloadScraped(urlElement) {
    // console.log(urlElement)
    let promise = new Promise(async function(resolve, reject) {
      const url = urlElement.url;
      let requestContent = {
        url: "https://api.webscraper.io/api/v1/scraping-job/" + urlElement.CreateScrapingJob + "/json?api_token=MAGNtHEXFM8WEJbJ68IMsusQpwcI635yEMUNwzCA8ZCRF1fA8z3XSu6mz5My",
        json: true,
      }; {
        request.get(requestContent, async function(error, response, body) {
          if(error) {
            console.log(error);
          }
          let carObj = new Object();
          const $ = cheerio.load(body.title);
          let musicLink = $("music-link a").attr("href");
          let albumsNo = musicLink.search("/albums/");
          let trackAsinNo = musicLink.search("trackAsin");
          let trackAsinNum = musicLink.search("trackAsin=");
          carObj.name = $("music-detail-header").attr("headline");
          carObj.trackPosition = $("music-image-row").text().charAt(0);
          carObj.image = $("music-detail-header").attr("image-src");
          let urlLocationData = getLocation(url);
          let playlistsNo = urlLocationData.pathname.search("/playlists/");
          carObj.isoCountry = urlLocationData.hostname.slice(-2);
          carObj.url = url;
          carObj.MusicContainerIdDsp = urlLocationData.pathname.substring(playlistsNo + 11, urlLocationData.pathname.length);
          carObj.idAlbumDsp = musicLink.substring(albumsNo + 8, trackAsinNo - 1);
          carObj.IdTrackDsp = musicLink.substring(trackAsinNum + 10, musicLink.length);
          resolve(carObj);
        });
      }
    });
    return promise;

    function getLocation(href) {
      var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
      return(match && {
        href: href,
        protocol: match[1],
        host: match[2],
        hostname: match[3],
        port: match[4],
        pathname: match[5],
        search: match[6],
        hash: match[7],
      });
    }
  }

  async createExcelFile(downloadScraped) {
    console.log(downloadScraped);
    let promise = new Promise(async function(resolve, reject) {
      const headingColumnNames = ["Name", "Track Position In playlist", "ImageUrl", "isoCountry", "Uri", "MusicContainerIdDsp", "idAlbumDsp", "IdTrackDsp", ];
      //Write Column Title in Excel file
      let headingColumnIndex = 1;
      headingColumnNames.forEach((heading) => {
        ws.cell(1, headingColumnIndex++).string(heading);
      });
      //Write Data in Excel file
      let rowIndex = 2;
      downloadScraped.forEach((record) => {
        let columnIndex = 1;
        Object.keys(record).forEach((columnName) => {
          ws.cell(rowIndex, columnIndex++).string(record[columnName]);
        });
        rowIndex++;
      });
      wb.write("Playlists.xlsx");
    });
    return promise;
  }
}

let indexModel = new IndexModel();
module.exports = indexModel;

