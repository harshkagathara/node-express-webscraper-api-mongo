const mongoose = require('mongoose');
const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

const scrappSchema = mongoose.Schema({
    name:String,
    trackPosition:Number,
    image:String,
    isoCountry : String,
    url : String,
    MusicContainerIdDsp: String,
    IdTrackDsp: String,
    idAlbumDsp: String,
    idScrapp : ObjectId,
    Date: String
}, {
    timestamps: true
});

module.exports = mongoose.model('scrapedData', scrappSchema);