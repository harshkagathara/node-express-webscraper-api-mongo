const mongoose = require('mongoose');

const scrappSchema = mongoose.Schema({
    url:String,
    CreateSitemapId:Number,
    CreateScrapingJob:Number,
    isDeleted : Number,
    status : String,
    Date: String
}, {
    timestamps: true
});

module.exports = mongoose.model('scrapp', scrappSchema);