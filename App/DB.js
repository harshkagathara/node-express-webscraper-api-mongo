const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

if(process.env.NODE_ENV === 'development'){
    mongoose.connect('mongodb://localhost:27017/scrapp' , {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }).then(() => {
       
        console.log("Successfully connected to the database from LocalHost");
    }).catch(err => {
        console.log('Could not connect to the database. Exiting now...', err);
        process.exit();
    });
      
}
