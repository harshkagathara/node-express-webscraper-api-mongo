const indexModel = require("../models/index.model");
const cheerio = require("cheerio");
const request = require("request");
const fs = require("fs");
const path = require("path");
const writeXlsxFile = require("write-excel-file/node");
const console = require("console");

exports.Index = async(req, res) => {
	
	const urlArr = await indexModel.getDataFromXlsxFile();
	if(urlArr != undefined && urlArr.length > 0) {
		for(const urlElement of urlArr) {
			let urlValid = await indexModel.checkUrlValidOrNot(urlElement);
			if(urlValid.status == 200) {
				let CreateSitemapData = await indexModel.CreateSitemap(urlValid.url);
				if(CreateSitemapData.success) {
					if(CreateSitemapData.data != undefined && CreateSitemapData.data.id != undefined) {
						let CreateScrapingJobData = await indexModel.CreateScrapingJob(CreateSitemapData.data.id, urlValid.url);
						if(CreateScrapingJobData.success) {
							if(CreateScrapingJobData.data != undefined && CreateScrapingJobData.data.id != undefined) {
								scrapDataObj = new Object();
								scrapDataObj.url = urlValid.url;
					 			scrapDataObj.CreateSitemapId = CreateSitemapData.data.id;
								scrapDataObj.CreateScrapingJob = CreateScrapingJobData.data.id;
                                scrapDataObj.isDeleted = 0;
								// scrapDataArr.push(scrapDataObj)
                                const urlArr = await indexModel.SaveScrepData(scrapDataObj);
							}
						}
					}
				} else {
					console.error(CreateSitemapData);
				}
			} else {
				console.error("url not valid");
			}
		}
			res.send("done")
		
	} else {
		console.error("data not found");
	}
};

exports.ScrapData = async(req, res) => {
    const urlArr = await indexModel.readScrappData();
 
	let updatedScrapingJob = new Array();
    if(urlArr != undefined && urlArr.length > 0 )
    {
		for(const urlElement of urlArr) {
			const GetScrapingJobData = await indexModel.GetScrapingJob(urlElement.CreateScrapingJob);

			if( GetScrapingJobData != undefined && GetScrapingJobData.success){
				const updateScrapedData = await indexModel.updateStatus(urlElement, GetScrapingJobData.data.status);
				updatedScrapingJob.push(updateScrapedData);
			}

		}
	}
	res.send(updatedScrapingJob);
};

exports.scrapingJob = async (req, res)=>{
	const updatedScrapingJob = await indexModel.updatedcrapingJob();
	let finalData = new Array();
	if(updatedScrapingJob != undefined && updatedScrapingJob.length > 0 )
	{
		for(const ScrapingJobEle of updatedScrapingJob) {
			
			if(ScrapingJobEle.status == 'finished'){
				const downloadScrapedData = await indexModel.downloadScraped(ScrapingJobEle);
				 await indexModel.savaScrappedData(ScrapingJobEle,downloadScrapedData);
				 await indexModel.updateIsDelete(ScrapingJobEle);
				finalData.push(downloadScrapedData)
			}else{
				res.redirect('http://localhost:3000/scrapingJob');
			}
		}
	}


	await indexModel.createExcelFile(finalData);
	res.send("done");
}
exports.test = async(req,res)=>{
    
	function getLocation(href) {
		var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
		return match && {
			href: href,
			protocol: match[1],
			host: match[2],
			hostname: match[3],
			port: match[4],
			pathname: match[5],
			search: match[6],
			hash: match[7]
		}
	}
	 const aa =  getLocation("https://music.amazon.in/playlists/B08FTJKYBN?tag=linkfire-smarturl-in-21"); 
	 
	console.log( aa.hostname, aa.search)
}
