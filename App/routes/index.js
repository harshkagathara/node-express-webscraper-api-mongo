module.exports = (app) => {
    const indecController = require('../controllers/index.controller');
   

    app.get('', indecController.Index);
    app.get('/scrap', indecController.ScrapData);
    app.get('/test', indecController.test);
    app.get('/scrapingJob' , indecController.scrapingJob)
   
}